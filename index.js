
var mixin = {
	saveFeatureRequest: function(el, event, loading, data) {
		console.log(data)
		loading(true)
	},
	dayToday: function(){
		return moment().format('dddd, MMM D \'YY')
	},
	dateToday: function(){
		return moment().format('YYYY-MM-DD')
	}
};

function viewModel(mixin){
	this.today = mixin.dayToday()

	this.isLoading = ko.observable(false)
	this.feature_title = ko.observable()
	this.feature_description = ko.observable()
	this.client_name = ko.observable()
	this.request_priority = ko.observable(1)
	this.target_date = ko.observable(mixin.dateToday())
	this.product_area = ko.observable()

	this.submitForm = function(el, event) {
		let data = {
			feature_title: this.feature_title(),
			feature_description: this.feature_description(),
			client_name: this.client_name(),
			request_priority: this.request_priority(),
			target_date: this.target_date(),
			product_area: this.product_area()
		}
		mixin.saveFeatureRequest(el, event, this.isLoading, data)
	}
}

ko.applyBindings(new viewModel(mixin));